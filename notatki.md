# Instalacje

https://code.visualstudio.com/

https://nodejs.org/en/ -> LTS
https://sfconservancy.org/news/2020/jun/23/gitbranchname/
https://github.com/GitCredentialManager/git-credential-manager

node -v
v16.13.1

npm -v
8.1.2

https://gitforwindows.org/

git --version
git version 2.31.1.windows.1

chrome://version/
Google Chrome 100.0.4896.127

# GIT

https://stackoverflow.blog/2017/05/23/stack-overflow-helping-one-million-developers-exit-vim/
https://sfconservancy.org/news/2020/jun/23/gitbranchname/
https://github.com/GitCredentialManager/git-credential-manager


# Terminal
git bash

Ctrl+C - zamknij
Ctrl+L - wyczysc
ls - pokaz pliki
cd <katalog> - change dir
cd "<katalog ze spacjami>" - change dir
. - biezacy katalog
.. - katalog nadrzedny

less / more:
(END) - q - aby wyjsc

## Git Init 

git init

Initialized empty Git repository in C:/Projects/altkom-git-kwiecien/.git/

## first git commit

git commit

Author identity unknown

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

## Git config

git config --list

git config --global --list
<!-- C:\Users\<uzytkownik>\.gitconfig -->

git config --global user.email "mateusz@altkom.pl"
git config --global user.name "Mateusz Kulesza"

## Git commit 
git commit 

 nothing added to commit but untracked files present (use "git add" to track)
 
 (use "git add <file>..." to include in what will be committed)


## Git Add
git add notatki.md 
git add . 

## Git status
git status

## REmove from index
git rm --cached notatki.md

## Vim
git commit 
<!-- // Opening vim for editing -->
<!-- .git/COMMIT_EDITMSG -->
a - wprowadzanie
d - wytnij/usun linie
p - paste
ESC - wyjdz z edycji
:q - wyjdz
:q! - wyjdz bez zapisuwania
:w  - zapisz
:wq - zapisz i wyjdz

[master (root-commit) 2c599e1] Mój Pierwszy Commit
 1 file changed, 90 insertions(+)
 create mode 100644 notatki.md

## Git log
git log
git log HEAD
git log master
git log 2c599e1

git log -5
git log a0a066c..1d165f2
git log HEAD~4..HEAD~1
git log HEAD -- strona/index.html
git log HEAD -L10,20:strona/index.html
git log --stat HEAD -- strona/
git log --before="12 hours ago"
git log --grep="Git"
git log --all
git log --decorate
git log --graph
git log HEAD --all --decorate --graph

## Git Log Format

git log --format="short"
git log --format="medium"
git log --format="fuller"
git log --format="%h %an - %s"



## Git Show
git show 
git show HEAD
git show master
git show 2c599e18ae609050119b31d5800c28a15a0b003c
git show 2c599e18ae6090
git show 2c599e1

## Git Commit 
git commit -m "Opis commita"

## GIT Diff
git diff
git diff <commit> -- <path>
git diff <commit> <commit> -- <path>

## Git Add -Patch

(1/1) Stage this hunk [y,n,q,a,d,e,?]? ?
y - stage this hunk
n - do not stage this hunk
q - quit; do not stage this hunk or any of the remaining ones   
a - stage this hunk and all later hunks in the file
d - do not stage this hunk or any of the later hunks in the file
e - manually edit the current hunk
? - print help

## Git commit --amend
git commit --amend 
git commit --amend -m "new message"

git add pominiete_pliki
git commit --amend -m "new message"


## Git Reset
git reset <WhereToMove>
git reset --soft <WhereToMove>
git reset --mixed <WhereToMove>
git reset --hard <WhereToMove>

## Git Reflog
git reflog 
git reset HEAD@{1}


## Dangling commits --no-reflog
<!-- 
git fsck --no-reflog
Checking object directories: 100% (256/256), done.
dangling commit 0daea63d0667e4b928185b0d758414fd7b2d59c6
dangling commit 19402c8c08835430a2b98ee200bc1c831192fba3
dangling commit 2b905bfde833e3c60a6b0eb58693ebf1dc398419
 -->

## Git Tag

git tag nazwa

git tag -l -n

git tag -a -m "Alpha version testing" v0.3-alpha

git show v0.3-alpha

<!-- 
tag v0.3-alpha
Tagger: Mateusz Kulesza <mateusz@altkom.pl>
Date:   Fri Apr 22 14:26:12 2022 +0200

Alpha version testing

commit 78acd4151b70d6ea16e296104846650ddc3fe6f4 (HEAD -> master, tag: v0.3-alpha)
Author: Mateusz Kulesza <mateusz@altkom.pl>
Date:   Fri Apr 22 14:22:34 2022 +0200 -->

## Git branch

<!-- utworz -->
git branch <nazwa-brancha>

<!-- przelacz na  -->
git checkout <nazwa-brancha>
git switch  <nazwa-brancha>

<!-- utworz i przelacz -->
git checkout -b <nazwa-brancha>
git switch -c <nazwa-brancha>


git switch -c release_0.3 master
git switch -c release_0.3 HEAD
git switch -c release_0.3 

git switch -c <nazwa-brancha> <z_ktorego_commita>

## Git Merge
git merge <co_dolaczyc_tutaj>


git merge --ff <co_dolaczyc_tutaj>
<!-- * 0c56f2e (HOTFIX_rodo) Done - Stopka Rodo
| * 96efc4d (WIP_feature_logo) WIP - Feature - Logo - nicolas cage ;-)
|/
* b629f5d (HEAD -> release_0.3, master) Git Branch
* d806192 Tag vs Annotated Tag
* 78acd41 (tag: v0.3-alpha) Git Tag -->

<!-- Cofamy z git reset  b629f5d -->

git merge --no-ff <co_dolaczyc_tutaj>
<!-- * f2a6ae1 (HEAD -> release_0.3) Git Merge
| * 0c56f2e (HOTFIX_rodo) Done - Stopka Rodo
|/
| * 96efc4d (WIP_feature_logo) WIP - Feature - Logo - nicolas cage ;-)
|/
* b629f5d (master) Git Branch
* d806192 Tag vs Annotated Tag -->

## Zdalna

git clone <skad> <dokad>

git remote
git remote show origin
git remote add <nazwa> <skad>

git fetch origin master
git fetch origin 
<!-- git status ( 1 behind ) -->

git branch -v -a 
<!-- *** -->
<!-- /remote/origin/*** -->

<!-- fetch origin master + merge /remotes/origin/master -->
git pull origin ichbranch:mojbranch
git pull origin master:master
git pull origin master:HEAD
git pull origin master